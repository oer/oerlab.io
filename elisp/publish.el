;;; publish.el --- Publish HTML from Org sources with Gitlab Pages
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2017-2020,2022,2023 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;; Use settings from emacs-reveal, but convert to plain HTML.
;; emacs --batch --load elisp/publish.el --funcall org-publish-all

;;; Code:

;; Avoid update of emacs-reveal, enable stacktraces.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t)

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir)
  (condition-case nil
      ;; Either require package with above hard-coded location
      ;; (e.g., in docker) ...
      (require 'emacs-reveal)
    (error
     ;; ... or look for sibling "emacs-reveal".
     (add-to-list
      'load-path
      (expand-file-name "../../emacs-reveal/" (file-name-directory load-file-name)))
     (require 'emacs-reveal))))

(setq org-publish-project-alist
      (list
       (list "org"
             :base-directory "."
             :base-extension "org"
             :exclude "CONTRIBUTING"
             :publishing-function '(oer-reveal-publish-to-html)
             :publishing-directory "./public")
       (list "well-known"
             :base-directory ".well-known"
             :base-extension "json"
             :publishing-function 'org-publish-attachment
             :publishing-directory "./public/.well-known")
       (list "tdmorg"
             :base-directory "tdm"
             :base-extension "org"
             :publishing-function '(oer-reveal-publish-to-html)
             :publishing-directory "./public/tdm")
       (list "tdmjson"
             :base-directory "tdm"
             :base-extension "json"
             :publishing-function 'org-publish-attachment
             :publishing-directory "./public/tdm")
       (list "quizzes"
             :base-directory "quizzes"
             :base-extension "js"
             :publishing-function 'org-publish-attachment
             :publishing-directory "./public/quizzes")
       (list "audio"
	     :base-directory "audio"
	     :base-extension (regexp-opt '("ogg" "mp3"))
	     :publishing-directory "./public/audio"
	     :publishing-function 'org-publish-attachment)
       (list "index-css"
             :base-directory "."
             :include '("index.css")
             :exclude ".*"
             :publishing-function 'org-publish-attachment
             :publishing-directory "./public")
       (list "favicon"
             :base-directory "."
             :include '("favicon.ico" "favicon.png")
             :exclude ".*"
             :publishing-function 'org-publish-attachment
             :publishing-directory "./public")
       (list "robots"
             :base-directory "."
             :include '("robots.txt")
             :exclude ".*"
             :publishing-function 'org-publish-attachment
             :publishing-directory "./public")
       (list "oer-reveal-css-resources"
             :base-directory (concat (file-name-as-directory
				      (expand-file-name oer-reveal-dir))
                                     "css")
             :base-extension (regexp-opt '("css" "png" "jpg" "ico" "svg" "gif"))
             :publishing-function 'org-publish-attachment
             :publishing-directory "./public/reveal.js/dist/theme")))
