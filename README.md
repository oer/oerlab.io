<!--- Local IspellDict: en -->
<!--- Copyright (C) 2019 Jens Lechtenbörger -->
<!--- SPDX-License-Identifier: CC0-1.0 -->

This project creates an [HTML index page](https://oer.gitlab.io/) for
projects in group [oer](https://gitlab.com/oer).
