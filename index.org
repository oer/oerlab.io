# Local IspellDict: en
#+SPDX-FileCopyrightText: 2019-2024 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0
#+STARTUP: showeverything

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="./reveal.js/dist/theme/index.css" />
#+HTML_HEAD: <link rel="shortcut icon" href="/favicon.ico" />
#+HTML_HEAD: <link rel="icon" type="image/png" href="/favicon.png" sizes="32x32" />

#+TITLE: Open Educational Resources on GitLab
#+AUTHOR: Jens Lechtenbörger
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil
#+KEYWORDS: open educational resources, OER, gitlab, creative commons, free software, emacs-reveal, reveal.js, presentations, slides
#+DESCRIPTION: Open Educational Resources (OER) as GitLab Pages with CI/CD pipelines based on free software

* Context
The [[https://gitlab.com/oer][group ~oer~ at GitLab]] collects
[[https://en.wikipedia.org/wiki/Open_educational_resources][Open
Educational Resources]] (OER).
In a nutshell, educational resources are /open/ if they are distributed
under [[https://en.wikipedia.org/wiki/Free_license][free licenses]]
(such as suitable variants of [[https://creativecommons.org/share-your-work/cclicenses/][Creative Commons licenses]]),
guaranteeing [[https://opencontent.org/blog/archives/3221][Wiley’s
5Rs of Openness]], namely the rights to
/reuse/, /redistribute/, /revise/, /remix/, and /retain/.
Consequently, OER promise to reduce entry barriers towards
educational resources as well as to avoid redundant work when similar
educational resources of high quality are created in different
organizations.  I explain
[[https://lechten.gitlab.io/talks-2019b/2019-10-22-OER.html][basics of OER in this OER presentation]];
an
[[https://electures.uni-muenster.de/engage/theodul/ui/core.html?id=bfd84252-634d-40d0-996a-3979a21abe3e][OER video recording (October 2019) entitled “Open Educational Resources: What, why, and how?” for that presentation is available.]]

According to the UNESCO’s [[https://open-educational-resources.de/wp-content/uploads/Ljubljana_OER_Action_Plan_2017.pdf][Ljubljana OER Action Plan 2017]]
OER can play a “pivotal role [...] toward achieving the 2030 Agenda
for Sustainable Development, and above all Sustainable Development
Goal 4 on Quality Education.”  However, several barriers have been
identified that hinder a widespread adoption of OER, and the Action
Plan proposes actions to overcome them, in particular for “building
the capacity of users to find, re-use, create and share OER.”

Subsequently, the Action Plan shaped the
[[https://www.unesco.org/en/legal-affairs/recommendation-open-educational-resources-oer][2019 UNESCO Recommendation on OER]],
which addresses five objectives with corresponding Areas of Action,
among which the first reads: “Building capacity of stakeholders to
create, access, re-use, adapt and redistribute OER”

The ~oer~ group of projects is well aligned with UNESCO’s Action Plan
and Recommendation in that it (a) respects the 5 Rs of openness
cite:Wil14 for Open Educational Resources, (b) observes the
requirements of the ALMS framework cite:HWS+10, and (c) additionally
employs best practices from software engineering cite:Lec19.
Currently, OER collected here were or are being created at the
[[https://www.wi.uni-muenster.de/][Department of Information Systems]]
of the
[[https://www.uni-muenster.de/en/][University of Münster, Germany]],
mostly with the
[[https://en.wikipedia.org/wiki/Free_and_open-source_software][free/libre
and open source software]]
(FLOSS) [[https://gitlab.com/oer/emacs-reveal][emacs-reveal]]
([[Technical Background][some details on emacs-reveal below]]);
additional OER projects based on free licenses are welcome.

Note that this site makes use of the
[[https://www.w3.org/2022/tdmrep/][W3C TDM Reservation Protocol (TDMRep)]]
to reserve rights related to text and data mining (TDM); see the
specification and [[file:tdm/licensing.org][that document]].

* Sample OER projects
- Current course
  - [[https://oer.gitlab.io/oer-courses/it-systems/][OER presentations for lectures on IT Systems]] ([[https://gitlab.com/oer/oer-courses/it-systems][source files]])
- Older courses
  - [[https://oer.gitlab.io/OS/][OER presentations for a course on Operating Systems]] ([[https://gitlab.com/oer/OS][source files for OER on OS]])
  - [[https://oer.gitlab.io/oer-courses/cacs/][OER presentations for lectures on Distributed Systems]] ([[https://gitlab.com/oer/DS][source files]])
  - In German: [[https://oer.gitlab.io/fediverse/][OER presentations for a course on the fediverse]] ([[https://gitlab.com/oer/fediverse][source files]])
  - Mix of German and English
    - [[https://oer.gitlab.io/oer-courses/vm-neuland/][OER presentations for a course surrounding Solid]]
      (summer term 2019, repeated in 2021)
      ([[https://gitlab.com/oer/oer-courses/vm-neuland][source files]])
    - [[https://oer.gitlab.io/oer-courses/vm-oer/][OER presentations for a course on Open Educational Resources (OER)]]
      (winter terms 2019/2020 and 2020/2021)
      ([[https://gitlab.com/oer/oer-courses/vm-oer][source files]])
- Presentations on selected topics such as
  - [[https://oer.gitlab.io/emacs-reveal-howto/howto.html][Howto for emacs-reveal]] ([[https://gitlab.com/oer/emacs-reveal-howto][source files]])
  - Presentations created by students as part of specialization module
    on OER
    - [[https://jonas_jostmann.gitlab.io/oer-meltdown/meltdown.html][Introduction to Meltdown]] ([[https://gitlab.com/jonas_jostmann/oer-meltdown][source files]])
    - [[https://chris-olbrich.gitlab.io/emacs-reveal-transaction-processing/transactionprocessing.html][Introduction to transaction processing]] ([[https://gitlab.com/chris-olbrich/emacs-reveal-transaction-processing][source files]])
    - In German, not using emacs-reveal: [[https://gitlab.com/oer/oer-db-normalization][Introduction to database normalization]]
  - [[https://oer.gitlab.io/misc/][Miscellaneous OER presentations]], including:
    - [[https://oer.gitlab.io/misc/Data-Quality.html][Introduction to Data Quality]] ([[https://gitlab.com/oer/misc][source files]])
    - [[https://oer.gitlab.io/misc/Regular-Expressions.html][Introduction to Regular Expressions]] ([[https://gitlab.com/oer/misc][source files]])
    - [[https://oer.gitlab.io/misc/Complexity-Example.html][Python example for Big O complexity]] ([[https://gitlab.com/oer/misc][source files]])
  - [[https://oer.gitlab.io/cs/functional-dependencies/functional_dependencies.html][A text on functional dependencies and 3NF normalization with the synthesis algorithm]]
    (generated from doc strings of [[https://gitlab.com/oer/cs/functional-dependencies/-/blob/master/functional_dependencies/functional_dependencies.py][this Python module]])
- OER explaining aspects of the infrastructure of emacs-reveal such as an
  [[https://oer.gitlab.io/oer-courses/vm-oer/03-Git-Introduction.html][Introduction to Git]]
  and an [[https://oer.gitlab.io/oer-courses/vm-oer/04-Docker.html][Introduction to Docker]]
- [[https://gitlab.com/oer/figures][OER figures used in some of the above presentations]]
- [[https://gitlab.com/oer/docker][Docker images]] for the
  infrastructure of emacs-reveal
- [[https://gitlab.com/oer/isii][Source code]] (using
  [[https://jekyllrb.com/][Jekyll]]) for my
  [[https://www.informationelle-selbstbestimmung-im-internet.de/][German Web page on digital self-defense for privacy in times of mass surveillance and surveillance capitalism]]

* Technical Background
For educational resources to be free and open, next to proper
licensing requirements also technical requirements exist (as defined
in the ALMS framework of cite:HWS+10, extended in cite:Lec19):

- OER should be usable (for learning) with
  [[https://en.wikipedia.org/wiki/Free_and_open-source_software][FLOSS]]
  on (almost) any device, also mobile and offline.
- OER should be editable with FLOSS
  (this requires source file access).
- OER should be re-usable under the Single Sourcing paradigm (see
  cite:Roc01), which enables reuse and revision from a single,
  consistent source without copy&paste (copy&paste creates isolated
  copies, where the reconciliation of changes and improvements by
  different individuals would be almost impossible).
- OER should offer a separation of contents from layout (then, experts
  for content do not need to be design experts as well; also,
  cross-organizational collaboration is supported where each
  organization can apply its own design guidelines).
- OER should be defined in a lightweight markup language, which is easy
  to learn and which enables the use of industrial-strength version
  control systems such as Git for the management of OER collaboration
  (comparison, revision, merge).

The above requirements are met by
[[https://gitlab.com/oer/emacs-reveal][emacs-reveal]] cite:Lec19-jose.
Emacs-reveal supports the creation of
OER presentations (HTML slides with audio explanations and
PDF variants) from source files
that are written in [[https://orgmode.org/][Org Mode]]
as lightweight markup language (see
cite:SD11 for a general introduction to Org Mode), which can be read
and written as text files in any text editor.  However, an editor with
“real” support is recommended to benefit from Org Mode’s features.
The native text editor for Org Mode is
[[https://www.gnu.org/software/emacs/][GNU Emacs]] but
[[https://orgmode.org/tools.html][other tools]]
support Org Mode as well.

With emacs-reveal, HTML presentations based on
[[https://revealjs.com/][reveal.js]] are generated from Org source
files.  Thanks to features of the HTML presentation framework
~reveal.js~ (with plugins) and Org mode, usual and advanced
presentation features are provided (e.g., animations and slide
transitions; speaker’s view with preview, notes, and timer; embedding
of images, audio, video, mathematical formulas; table of contents;
bibliography; keyword index; hyperlinks within and between
presentations; themes for different styling; responsive design with
touch support; quizzes for retrieval practice; code highlighting and
evaluation for programming languages).  Besides, emacs-reveal allows
generating concise PDF versions from Org source files (in addition to
the PDF export of reveal.js).

With emacs-reveal, license attribution for OER figures is simplified,
with machine-readable licensing information represented in RDFa in
HTML cite:Lec19-delfi.  Besides, emacs-reveal supports the specification
of an OER’s primary license with
[[https://reuse.software/][SPDX headers as used in the REUSE project]],
from which license information can be generated (“Except
where otherwise noted, the work x by y is published under the license
z”), e.g., the note at the bottom of this HTML file as well as
[[https://oer.gitlab.io/emacs-reveal-howto/howto.html#slide-license][the final slide of the howto for emacs-reveal]],
display generated license information in human-readable form /and/
contain machine-readable RDFa markup (PDF versions just contain
human-readable variants).

For the above OER presentations, the generation of ~reveal.js~
presentations from Org sources happens automatically in a continuous
integration infrastructure of GitLab based on a special Docker image
(about details of which the users of emacs-reveal do not need to know
anything).

* Contributing
Your contributions are very welcome!  Please check out some hints in a
[[https://gitlab.com/oer/oer.gitlab.io/blob/master/CONTRIBUTING.org][separate document]].

* Bibliography
  :PROPERTIES:
  :CUSTOM_ID: bibliography
  :UNNUMBERED: t
  :END:

printbibliography:references.bib
